//
//  ViewController.swift
//  PDFKitPoC
//
//  Created by Dhanashri Patil on 23/08/18.
//  Copyright © 2018 Cybage. All rights reserved.
//

import UIKit
import PDFKit

let IS_IPHONE = UI_USER_INTERFACE_IDIOM() == .phone
let IS_IPHONE_X = IS_IPHONE && UIScreen.main.bounds.size.height == 812.0


class ViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
  
    var pdfView:PDFView?
    var pdfDocument:PDFDocument?
    var thumbnailView:PDFThumbnailView?
    var pdfPage:PDFPage!
    let mutableArray = NSMutableArray()
    let animationDuration: TimeInterval = 0.25
    var selectedIndexPath: IndexPath?

    @IBOutlet weak var pdfContainerView: UIView!
    @IBOutlet weak var topToolbar: UIToolbar!
    @IBOutlet weak var bottomThumbnailView: UIView!
    @IBOutlet weak var thumbnailCollectionView: UICollectionView!
    
    @IBOutlet weak var bottomThumbnailViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var topToolbarHeightConstraint: NSLayoutConstraint!


    override func viewDidLoad() {
        super.viewDidLoad()
        topToolbarHeightConstraint.constant = IS_IPHONE_X ? 100 : 80
        bottomThumbnailViewHeightConstraint.constant = IS_IPHONE_X ? 100 : 80
        setupPDFView()
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.toggleTopBottomView))
        pdfContainerView.addGestureRecognizer(tap)
      //  topToolbar.isHidden = true
      //  bottomThumbnailView.isHidden = true
        
        thumbnailCollectionView.dataSource = self
        thumbnailCollectionView.delegate = self
        thumbnailCollectionView.register(UINib(nibName: "ThumbnailCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ThumbnailCollectionViewCell")

    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.updateThumbnailCollectionForSelectedIndex()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(self.pdfViewPageChangedNotification(_:)), name: .PDFViewPageChanged, object: pdfView)
        NotificationCenter.default.addObserver(self, selector: #selector(self.pdfViewAnnotationHitNotification(_:)), name: .PDFViewAnnotationHit, object: pdfView)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: .PDFViewPageChanged, object: pdfView)
        NotificationCenter.default.removeObserver(self, name: .PDFViewAnnotationHit, object: pdfView)
    }

    
    func setupPDFView() {
        
        pdfContainerView.frame = view.frame
        pdfView = PDFView(frame: pdfContainerView.bounds)
        pdfView?.autoresizingMask = [.flexibleWidth, .flexibleHeight, .flexibleTopMargin, .flexibleBottomMargin]
        pdfView?.autoScales = true
        pdfView?.maxScaleFactor = 4.0
        pdfView?.minScaleFactor = (pdfView?.scaleFactorForSizeToFit)!
        pdfView?.displayMode = .singlePage
        pdfView?.displayDirection = .horizontal
        pdfView?.zoomIn(self)
        pdfContainerView.addSubview(pdfView!)
        
        let url: URL? = Bundle.main.url(forResource: "geographic", withExtension: "pdf")
        if let anUrl = url {
            pdfDocument = PDFDocument(url: anUrl)
        }
        
        pdfView?.document = pdfDocument
        pdfView?.usePageViewController(true, withViewOptions: [UIPageViewControllerOptionInterPageSpacingKey:15])
    }
    

    // MARK: - Toggle Top/Bottom View
    @objc func toggleTopBottomView() {
        
        UIView.transition(with: bottomThumbnailView, duration: 0.3, options: .transitionCrossDissolve, animations: {
            self.bottomThumbnailView.isHidden = !self.bottomThumbnailView.isHidden
        })
        UIView.transition(with: topToolbar, duration: 0.3, options: .transitionCrossDissolve, animations: {
            self.topToolbar.isHidden = !self.topToolbar.isHidden
        })
    }
    
   // MARK: - collection view delegate methods

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Int(pdfDocument!.pageCount ) > 0 ? (pdfDocument!.pageCount ) : 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ThumbnailCollectionViewCell", for: indexPath) as? ThumbnailCollectionViewCell
        
        let pdfPage: PDFPage? = pdfDocument?.page(at: indexPath.item)
        if pdfPage != nil {
            let thumbnail: UIImage? = pdfPage?.thumbnail(of: (cell?.bounds.size)!, for: .cropBox)
            cell?.pdfThumbnail.image = thumbnail
            cell?.pdfPageNumber.text = String(format: "%ld", indexPath.item + 1)
        }
        
        if (pdfView?.currentPage?.isEqual(pdfPage))! {
            cell?.setHighlighted(true)
            cell?.pdfPageNumber.text =  pdfView?.currentPage?.label
        } else {
            cell?.setHighlighted(false)
        }

        
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let pdfPage: PDFPage? = pdfDocument?.page(at: indexPath.item)
        
        if let aPage = pdfPage {
            pdfView?.go(to: aPage)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 52, height: 76)
    }
    
    // MARK: - update bottom view
    func updateThumbnailCollectionForSelectedIndex() {
        var row: Int? = nil
        if let aPage = pdfView?.currentPage {
            row = pdfDocument?.index(for: aPage)
        }
        if (self.selectedIndexPath != nil) {
            if let aPath = [selectedIndexPath] as? [IndexPath] {
                thumbnailCollectionView.reloadItems(at: aPath)
            }
        }
        selectedIndexPath = IndexPath(row: row ?? 0, section: 0)
        if let aPath = [selectedIndexPath] as? [IndexPath] {
            thumbnailCollectionView.reloadItems(at: aPath)
        }
        if !thumbnailCollectionView.indexPathsForVisibleItems.contains(selectedIndexPath!) {
            thumbnailCollectionView?.scrollToItem(at: self.selectedIndexPath!, at: .centeredHorizontally, animated: true)
        }
    }
    
    // MARK: - PDFViewPageChangedNotification
    @objc func pdfViewPageChangedNotification(_ notification: Notification?) {
        updateThumbnailCollectionForSelectedIndex()
    }
    
    // MARK: - PDFViewAnnotationHitNotification
    @objc func pdfViewAnnotationHitNotification(_ notification: Notification?) {
        let annotation = notification?.userInfo!["PDFAnnotationHit"] as? PDFAnnotation
        var pageNumber: Int? = nil
        if let aPage = annotation?.destination?.page {
            pageNumber = pdfDocument?.index(for: aPage)
        }
        print(String(format: "Page: %lu", UInt(pageNumber ?? 0)))
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
   
}

