//
//  ThumbnailCollectionViewCell.swift
//  PDFKitPoC
//
//  Created by Dhanashri Patil on 28/08/18.
//  Copyright © 2018 Cybage. All rights reserved.
//

import UIKit

class ThumbnailCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var pdfThumbnail: UIImageView!
    @IBOutlet weak var pdfPageNumber: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setHighlighted(_ highlighted: Bool) {
        if highlighted {
            layer.borderColor = UIColor(red: 21 / 255.0, green: 120 / 255.0, blue: 237 / 255.0, alpha: 1.0).cgColor
            layer.borderWidth = 2.0
        } else {
            layer.borderColor = UIColor.clear.cgColor
        }
    }

}
